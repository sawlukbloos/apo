/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import DAO.DAOLogin;

/**
 *
 * @author David Noguera y  Samuel Bolaños
 * Implementacion del DAO en el controlador junto al usuario y contrasenia
 */
public class ControladorLogin implements DAOLogin{

    private static ControladorLogin controladorLogin;
    private String usuario;
    private String contrasenia;
    
    

    private ControladorLogin() {
        
    }
    /**
     * Aplicacion de un patron singleton para un mejor manejo de recursos.
     */
    public static ControladorLogin getControladorLogin(){
        if(controladorLogin==null)
            controladorLogin=new ControladorLogin();
        
        return controladorLogin;
    }
    
    /**
     * 
     * @param usuario
     * @param contrasenia
     * @return estado 
     * Se realiza una verfiacion de usuario y contraseña , notese el uso de "Override" tras haberse implementado el DAO
     */
    @Override
    public boolean VerificarUsuarios(String usuario, String contrasenia) {
        boolean estado=false;
        
        if((usuario.equals("daniel"))&&(contrasenia.equals("12345")))
            estado=true;
        
        return estado;
    }
   
    
    @Override
    public String getUsuario() {
        return this.usuario;
    }

    @Override
    public boolean CambiarContrasenia(String contrasenia) {
        boolean estado=true;        
        this.contrasenia=contrasenia;
        return estado;
        
    }
    
    
    
    
    
}
