/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

/**
 *
 * @author sistemas
 */
public class Operaciones {
    
    public String Sumar(int numero1, int numero2){
        float resultado = numero1+numero2;
        return String.valueOf(resultado);   
    }
    
    public String Restar(int numero1, int numero2){
        float resultado = numero1-numero2;
        return String.valueOf(resultado);   
    }
    
    public String Multi(int numero1, int numero2){
        float resultado = numero1*numero2; 
        return String.valueOf(resultado);   
    }
    
   public String Div(int numero1, int numero2){
        if (numero2 == 0) {
        String mensaje =("No se puede dividir entre cero");        
        return mensaje;
                }else{ 
        float resultado = numero1 / numero2;               
        return String.valueOf(resultado);
     }
    }
    
 }

