import java.util.Scanner;
import javax.swing.JOptionPane;

public class Gato {

  //Definiendo variables  
  private String nombre;
  private String raza;
  private int peso;
  private String estado;
  private int edad;
  
  public Gato() {}
  public Gato(String nombre, String raza, int peso, String estado, int edad) {
   
    this.nombre = nombre;
    this.raza = raza;
    this.peso = peso;
    this.estado = estado;
    this.edad =edad;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public void setRaza(String raza) {
    this.raza = raza;
  }

  public void setPeso(int peso) {
    this.peso = peso;
  }

public void setEstado(String estado) {
    this.estado = estado;
  }
  public void setEdad(int edad) {
    this.edad = edad;
  }


  public String getNombre() {
    return nombre;
  }

  public String getRaza() {
    return raza;
  }

  public int getPeso() {
    return peso;
  }

  public String getEstado() {
    return estado;
  }

  public int getEdad() {
    return edad;
  }


   public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Gato gato = new Gato();

    JOptionPane.showMessageDialog(null, "Bienvenido al simulador de Gato :)");
    //seteando gato 
    String nombre = JOptionPane.showInputDialog(null, "Ingrese el nombre del gato:");
    gato.setNombre(nombre);
    
    String raza = JOptionPane.showInputDialog(null, "Ingrese el raza del gato:");
    gato.setRaza(raza);

    int peso = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese el peso del gato:"));
    gato.setPeso(peso);

    int edad = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese la edad del gato"));
    gato.setEdad(edad);

    String estado = JOptionPane.showInputDialog(null, "Introduce el estado de animo del gato");
    gato.setEstado(estado);

    sc.close();
    //RESULTADOS
    JOptionPane.showMessageDialog(null, "Nombre: " + gato.getNombre() +
        "\nRaza: " + gato.getRaza() +
        "\nPeso: " + gato.getPeso() +
        "\nEstado: " + gato.getEstado() +
        "\nEdad: " + gato.getEdad());
  }
  
   


    
}
