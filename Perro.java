import java.util.Scanner;
public class Perro {

    private String nombre;
    private String color;
    private float peso;
    private float tamanio;
    private String estado;

    public Perro(){}

    public Perro(String nombre, String color, float peso, float tamanio, String estado){

        this.nombre=nombre;
        this.color=color;
        this.peso=peso;
        this.tamanio=tamanio;
        this.estado=estado;

    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getNombre() {
        return nombre;
    }

    public void setColor(String color) {
        this.color = color;
    }
    public String getColor() {
        return color;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }
    public float getPeso() {
        return peso;
    }

    public void setPeso(float tamanio) {
        this.tamanio = tamanio;
    }
    public float getTamanio() {
        return tamanio;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    public String getEstado() {
        return estado;
    }




public static void main(String[] args) {
Scanner sc = new Scanner(System.in);
Perro perro = new Perro();

System.out.print("Introduce el nombre del perro: ");
 perro.setNombre(sc.nextLine());

System.out.print("Introduce el color del perro: ");
 perro.setColor(sc.nextLine());

System.out.print("Introduce el peso del perro : ");
 perro.setPeso(sc.nextFloat());

System.out.print("Introduce el tamanio del perro : ");
 perro.setTamanio(sc.nextFloat());

System.out.print("Que esta haciendo el perro: ");
 perro.setEstado(sc.nextLine());

sc.close();

System.out.println("Nombre: " + perro.getNombre());
System.out.println("Color: " + perro.getColor());
System.out.println("El peso del perrito es: " + perro.getPeso());
System.out.println("El tamanio del perrito es: " + perro.getTamanio());
System.out.println("El perro esta: " + perro.getEstado());
    }
    
}
